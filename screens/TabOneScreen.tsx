import * as React from 'react';
import { StyleSheet, ImageBackground, ScrollView, Image, Dimensions } from 'react-native';
import { Text, View } from '../components/Themed';
import { Entypo, FontAwesome5, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import { Col, Row, Grid } from "react-native-easy-grid";
import { FavoriteField } from '../components/FavoriteField';
import Layout from '../components/Layout';

export default function TabOneScreen() {
  return (
    <Layout>
      <View style={styles.avatarContainer} >
        <Image source={require('../assets/images/me.jpeg')} style={styles.avatar} />
      </View>
      <Text style={[styles.text, styles.name]}>Sajida Mahmoud</Text>
      <Text style={[styles.text, styles.title]}>Drown into Passion, Driven by Goals.</Text>
      <View style={styles.textContainer}>
        <Text style={[styles.text, styles.content]}>My Name is Sajida Mahmoud. I am a web developer with unmentionable expericne but with huge dreams and will.
        This is my first mobile application. If you want to know more about me, do not hesitate to contact!
            </Text>
        <View style={styles.iconsContainer}>
          <Entypo name="facebook" style={styles.icon} />
          <Entypo name="email" style={styles.icon} />
          <Entypo name="linkedin" style={styles.icon} />
        </View>
      </View>
      <View style={styles.textContainer}>
        <Text style={[styles.text, styles.subTitle]}>Interests</Text>
      </View>
      <Grid>
        <Row style={styles.row}>
          <Col style={[styles.column, styles.col11]}>
            <Text style={styles.interestText}>Reading</Text>
            <FontAwesome5 name="book" style={styles.interestIcon} />
          </Col>
          <Col style={[styles.column, styles.col12]}>
            <Text style={styles.interestText}>Music</Text>
            <FontAwesome name="music" style={styles.interestIcon} />
          </Col>
        </Row>
        <Row style={styles.row}>
          <Col style={[styles.column, styles.col21]}>
            <Text style={styles.interestText}>Drawing</Text>
            <FontAwesome name="paint-brush" style={styles.interestIcon} />
          </Col>
          <Col style={[styles.column, styles.col22]}>
            <Text style={styles.interestText}>Cooking</Text>
            <MaterialCommunityIcons name="silverware-fork-knife" style={styles.interestIcon} />
          </Col>
        </Row>
      </Grid>
      <View style={styles.textContainer}>
        <Text style={[styles.text, styles.subTitle]}>Fields I Love to Dive into</Text>
      </View>
      <FavoriteField title='Programming' content="It's not just my major, but also my favorite field of discovery." imageSource={require('../assets/images/program.png')} />
      <FavoriteField title='Sea Creatures' content="I love to always know about sea creatures. Blue whale is my favorite animal." imageSource={require('../assets/images/whales.jpg')} />
      <FavoriteField title='Physics and Science' content={`"Physics is a good framework of thinking." Elon Musk says, and I totally agree.`} imageSource={require('../assets/images/physics.jpg')} />
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  avatarContainer: {
    marginTop: 30,
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    backgroundColor: '#FFFFFF90',
    alignSelf: 'center',
    padding: 5,
  },
  avatar: {
    flex: 1,
    resizeMode: "cover",
    width: '100%',
    height: '100%',
    borderRadius: 100 / 2,
  },
  imageBackground: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  textContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF90',
    margin: 15,
    marginTop: 45,
    paddingVertical: 40,
    borderRadius: 15,
  },
  text: {
    color: '#4c4f8c',
    textAlign: 'center',
  },
  title: {
    fontSize: 10,
    lineHeight: 30,
    marginHorizontal: 20
  },
  name: {
    fontSize: 13,
    lineHeight: 30,
    marginHorizontal: 20
  },
  subTitle: {
    marginRight: 15,
    fontSize: 15,
    lineHeight: 24,
  },
  content: {
    marginRight: 15,
    fontSize: 12,
    lineHeight: 24,
    textAlign: 'right'
  },
  iconsContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  icon: {
    fontSize: 24,
    marginTop: 10,
    marginHorizontal: 5,
    color: '#4c4f8c',
    textAlign: 'center',
  },
  row: {
    flex: 1,
    marginHorizontal: 15,
    marginVertical: 5,
    justifyContent: "space-around"
  },
  column: {
    height: 150,
    width: 150,
    borderRadius: 15,
    alignItems: 'center',
  },
  col11: {
    backgroundColor: '#fca4d490',
  },
  col12: {
    backgroundColor: '#fcfba490',
  },
  col21: {
    backgroundColor: '#61ad6590',
  },
  col22: {
    backgroundColor: '#6195ed90',
  },
  interestIcon: {
    color: '#4c4f8c',
    textAlign: 'center',
    fontSize: 60
  },
  interestText: {
    color: '#4c4f8c',
    alignSelf: 'flex-end',
    margin: 15,
    fontSize: 10
  },
});
