import * as React from 'react';
import { StyleSheet, Button, TextInput, Alert } from 'react-native';
import Layout from '../components/Layout';
import { Formik } from 'formik';
import { Text, View } from '../components/Themed';
import CustomButton from '../components/CustomButton';
import * as yup from 'yup';
import Modal from 'react-native-modal';

export default function TabTwoScreen() {
  const [submitted, setSubmitted] = React.useState(false)

  return (
    <Layout>
      <Text style={styles.heading}>We can be friends!</Text>
      <Text style={styles.subHeading}> Just send
      me what you would love to know more about me, and be sure that I would
      also love to know about you, as well!</Text>
      <Formik
        initialValues={{ email: '', message: '' }}
        onSubmit={() => {
          setSubmitted(true)
        }}
        validationSchema={yup.object().shape({
          email: yup
            .string()
            .email()
            .required(),
          message: yup
            .string()
            .min(2)
            .required(),
        })}
      >
        {({ values, handleChange, errors, setFieldTouched, touched, handleSubmit }) => (
          <View style={styles.formContainer}>
            <Text style={styles.textTitle}>Email</Text>
            <TextInput
              value={values.email}
              onChangeText={handleChange('email')}
              onBlur={() => setFieldTouched('email')}
              placeholder="example@gmail.com"
              style={styles.inputField}
            />
            {touched.email && errors.email &&
              <Text style={{ fontSize: 10, color: 'red' }}>{errors.email}</Text>
            }

            <Text style={styles.textTitle}>Message</Text>
            <TextInput
              value={values.message}
              onChangeText={handleChange('message')}
              onBlur={() => setFieldTouched('message')}
              secureTextEntry={true}
              style={styles.textArea}
              multiline={true}
              numberOfLines={6}
            />
            {touched.message && errors.message &&
              <Text style={{ fontSize: 10, color: 'red' }}>{errors.message}</Text>
            }
            <CustomButton onPress={() => handleSubmit()} title="Send Message" />
            <Modal isVisible={submitted}>
              <View>
                <Text style={styles.modalText}>Thank you! Will contact you back soon</Text>
                <View style={styles.modalButton}>
                  <CustomButton title="Ok" onPress={() => setSubmitted(false)} />
                </View>
              </View >
            </Modal>
          </View>
        )}
      </Formik>
    </Layout >
  );
}

const styles = StyleSheet.create({
  formContainer: {
    paddingHorizontal: 20,
    paddingVertical: 0,
    backgroundColor: 'transparent'
  },
  modalButton: {
    paddingHorizontal: 50,
  },
  modalContainer: {
    borderRadius: 15,
  },
  inputField: {
    fontSize: 10,
    borderRadius: 15,
    height: 50,
    backgroundColor: '#FFFFFF',
    textDecorationLine: 'none',
    color: '#4c4f8c',
  },
  textArea: {
    fontSize: 10,
    borderRadius: 15,
    backgroundColor: '#FFFFFF',
    textDecorationLine: 'none',
    textAlign: 'left',
    textAlignVertical: 'top',
    paddingTop: 10,
    color: '#4c4f8c'
  },
  submitButton: {
    color: '#4c4f8c',
    borderRadius: 15,
    flex: 1,
    justifyContent: "center",
  },
  heading: {
    fontSize: 13,
    lineHeight: 20,
    marginHorizontal: 20,
    marginTop: 70,
    color: '#4c4f8c',
    textAlign: 'center',
  },
  subHeading: {
    fontSize: 10,
    lineHeight: 20,
    marginHorizontal: 20,
    color: '#4c4f8c',
    textAlign: 'center',
  },
  textTitle: {
    fontSize: 10,
    lineHeight: 20,
    color: '#4c4f8c',
    textAlign: 'right',
  },
  modalText: {
    fontSize: 13,
    lineHeight: 20,
    color: '#4c4f8c',
    textAlign: 'center',
  },
});
