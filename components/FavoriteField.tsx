import * as React from 'react';

import { Text, View } from './Themed';
import { Image, StyleSheet } from 'react-native';

interface FavoriteFieldProps {
  title: String;
  content: String;
  imageSource: any
}
export function FavoriteField({ title, content, imageSource }: FavoriteFieldProps) {
  return (
    <View style={styles.fieldContainer}>
      <View style={styles.fieldContentContainer}>
        <Text style={[styles.text, styles.filedTitle]}>{title}</Text>
        <Text style={[styles.text, styles.fieldContent]}>{content}</Text>
      </View>
      <View style={styles.fieldImageContainer}>
        <Image source={imageSource} style={styles.fieldImage} />
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  fieldContainer: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 15,
    marginVertical: 5,
    borderRadius: 15,
    height: 150,
  },
  fieldImageContainer: {
    width: '45%',
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15
  },
  fieldImage: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  fieldContentContainer: {
    width: '55%',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  filedTitle: {
    fontSize: 10,
    lineHeight: 15,
    textAlign: 'right',
    marginBottom: 5,
    color: '#a1a1a1'
  },
  fieldContent: {
    fontSize: 11,
    lineHeight: 15,
    textAlign: 'right',
  },
  text: {
    color: '#4c4f8c',
    textAlign: 'center',
  },
});