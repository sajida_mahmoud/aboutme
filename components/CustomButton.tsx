import React from "react"
import { StyleSheet, TouchableOpacity, Text, GestureResponderEvent } from "react-native"

export default function CustomButton({ onPress, title }: { onPress: (event: GestureResponderEvent) => void, title: String }) {
  return (
    <TouchableOpacity onPress={onPress} style={styles.appButtonContainer}>
      <Text style={styles.appButtonText}>{title}</Text>
    </TouchableOpacity>)
}

const styles = StyleSheet.create({
  appButtonContainer: {
    backgroundColor: "#11909999",
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderRadius: 15,
    marginVertical: 10,
  },
  appButtonText: {
    fontSize: 13,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "none"
  }
});