import * as React from 'react';
import { StyleSheet, ImageBackground, ScrollView, Dimensions } from 'react-native';
import { View } from '../components/Themed';

interface LayoutProps {
  children: React.ReactNode
}
export default function Layout({ children }: LayoutProps) {
  return (
    <View style={styles.container}>
      <ImageBackground source={require('../assets/images/bgg.jpg')} style={styles.imageBackground}>
        <ScrollView>
          {children}
        </ScrollView>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  imageBackground: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    paddingBottom: 45
  },
});
